let collection = ["a","b","c","d","e"];

function addToStack(element){

	//What array method or how can we add an element/item at the end of our array?
	//at the top of our stack?
	collection.push(element)

	//return the updated collection:
	return collection;

}

addToStack('Chocolate');

function removeFromStack(){

	//What array method or how can we remove the last item in the array?
	//let One In, First One Out:
	collection.pop();

	//return the updated collection
	return collection;

}

removeFromStack();//Chocolate was the last one in, it will be the first one out.

function peek(){

	//return the topmost item in the stack/the last item in the array:
	return collection[collection.length-1];

}

peek();

function getLength(){

	//return the total number of items in the stack/array:
	return collection.length

}

getLength();